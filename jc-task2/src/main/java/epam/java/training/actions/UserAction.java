package epam.java.training.actions;

import epam.java.training.data.JewelryMetal;
import epam.java.training.jewelrystones.Stone;
import epam.java.training.utility.InputValidator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserAction {
    private static final BufferedReader READER = new BufferedReader(new InputStreamReader(System.in));
    private static final List<String> stoneTypesList = new ArrayList<>();

    static {
        stoneTypesList.add("Diamond");
        stoneTypesList.add("Pearl");
        stoneTypesList.add("Beryl");
        stoneTypesList.add("Emerald");
        stoneTypesList.add("Aquamarine");
    }

    public static void printWelcomeScreen() {
        System.out.println("***********************************");
        System.out.println("Welcome to Designer Jewelry Store!!!");
        System.out.println("***********************************");
        System.out.println();
    }

    public static Set<Stone> getUserChosenStones() throws IOException {
        Set<Stone> stoneSet = new HashSet<>();
        String userChosenStone;
        int stoneOrderNumber;

        System.out.println("Choose stone from list:");

        while (true) {
            System.out.println("-------------------------------");
            for (int i = 0; i < stoneTypesList.size(); i++) {
                System.out.println((i+1) + ". " + stoneTypesList.get(i));
            }
            System.out.println("-------------------------------");
            System.out.println("Enter stone order number: ");
            try {
                stoneOrderNumber = Integer.parseInt(READER.readLine());
                if (stoneOrderNumber < 0 || stoneOrderNumber > stoneTypesList.size()) {
                    System.out.println("Invalid stone order number. Try again");
                    continue;
                }

                userChosenStone = stoneTypesList.get(stoneOrderNumber - 1);
                Stone stone = StoneAction.getFilledStoneObject(userChosenStone);
                stoneSet.add(stone);

                System.out.println("Add more stones? y/n: ");
                if (READER.readLine().equalsIgnoreCase("n")) {
                    break;
                }

            } catch (NumberFormatException e) {
                System.out.println("Invalid Input. Try again");
            }
        }

        return stoneSet;
    }

    public static JewelryMetal getUserChosenJewelryMetal() throws IOException {
        return InputValidator.getValidMetal();
    }

    public static String getUserGivenJewelryName() throws IOException {
        return InputValidator.getValidString("jewelry name");
    }

    public static double getUserDefinedMetalWeight() throws IOException {
        return InputValidator.getValidDouble("metal weight (in gram)");
    }

    public static double getUserDefinedClarityLowerBound() throws IOException {
        double lowerBound;
        while (true) {
            try {
                System.out.println("Enter clarity lower bound (0.00-10.00):");
                lowerBound = Double.parseDouble(READER.readLine());
                if (lowerBound < 0 || lowerBound > 10) {
                    System.out.println("Value should be in range (0.00-10.00)");
                    continue;
                }
                break;
            } catch (NumberFormatException e) {
                System.out.println("Invalid Input. Try again");
            }
        }
        return lowerBound;
    }

    public static double getUserDefinedClarityUpperBound() throws IOException {
        double upperBound;
        while (true) {
            try {
                System.out.println("Enter clarity upper bound (0.00-10.00):");
                upperBound = Double.parseDouble(READER.readLine());
                if (upperBound < 0 || upperBound > 10) {
                    System.out.println("Value should be in range (0.00-10.00)");
                    continue;
                }
                break;
            } catch (NumberFormatException e) {
                System.out.println("Invalid Input. Try again");
            }
        }
        return upperBound;
    }

    public static void finishUserInteraction() throws IOException {
        InputValidator.getREADER().close();
        READER.close();
    }

}
