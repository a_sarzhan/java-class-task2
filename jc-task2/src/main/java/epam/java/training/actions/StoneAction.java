package epam.java.training.actions;

import epam.java.training.data.Country;
import epam.java.training.data.CuttingModel;
import epam.java.training.jewelrystones.*;
import epam.java.training.utility.InputValidator;

import java.io.IOException;

class StoneAction {

    private static void setStoneTotalCost(Stone stone) {
        double cuttingExpense = 0;
        double miningExpense;
        double expertsPreCost;

        if (stone instanceof Beryl) {
            ((Beryl) stone).generateHardness();
            ((Beryl) stone).generateTenacity();
        } else if (stone instanceof Diamond) {
            cuttingExpense = ((Diamond) stone).getMastersCuttingExpense();
        }

        miningExpense = stone.getMiningExpense();
        expertsPreCost = stone.getCostDefinedByExpertsSurvey();
        stone.calculateNetCost(miningExpense, expertsPreCost, cuttingExpense);

    }

    static Stone getFilledStoneObject(String userChosenStone) throws IOException {
        Stone stoneObject;

        String stoneName = InputValidator.getValidString(userChosenStone + " name");
        double carat = InputValidator.getValidDouble(userChosenStone + " carat");
        int amount = InputValidator.getValidInteger(userChosenStone + " amount");
        String color = InputValidator.getValidString(userChosenStone + " color");
        Country country = InputValidator.getValidCountry(userChosenStone);

        if (userChosenStone.equalsIgnoreCase("Diamond")) {

            CuttingModel model = InputValidator.getValidCuttingModel(userChosenStone);
            stoneObject = new Diamond(stoneName, carat, color, amount, model);

        } else if (userChosenStone.equalsIgnoreCase("Pearl")) {

            int pearlYear = InputValidator.getValidInteger(userChosenStone + " year");
            double pearlDiameter = InputValidator.getValidDouble(userChosenStone + " diameter (in centimeter)");
            stoneObject = new Pearl(stoneName, carat, color, amount, pearlDiameter, pearlYear);

        } else if (userChosenStone.equalsIgnoreCase("Aquamarine")) {
            stoneObject = new Aquamarine(stoneName, carat, color, amount);

        } else if (userChosenStone.equalsIgnoreCase("Emerald")) {
            stoneObject = new Emerald(stoneName, carat, color, amount);

        } else {
            stoneObject = new Beryl(stoneName, carat, color, amount);
        }

        stoneObject.defineStonesClarity();
        stoneObject.setCountry(country);
        setStoneTotalCost(stoneObject);

        return stoneObject;
    }

}
