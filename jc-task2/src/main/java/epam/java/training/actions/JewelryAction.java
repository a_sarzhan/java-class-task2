package epam.java.training.actions;

import epam.java.training.data.Jewelry;
import epam.java.training.jewelrystones.Stone;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class JewelryAction {

    public static void calculateAndSetJewelryTotalCost(Jewelry jewelry) {
        double jewelryCost = 0;
        double metalCost = jewelry.getMetal().getCost() * jewelry.getMetalWeight();

        for (Stone stone : jewelry.getStoneList()) {
            jewelryCost += stone.getCost();
        }

        jewelryCost += metalCost;
        jewelry.setJewelryTotalCost(jewelryCost);
    }

    public static void calculateAndSetJewelryTotalWeight(Jewelry jewelry) {
        double jewelryWeightInCarat = 0;

        for (Stone stone : jewelry.getStoneList()) {
            jewelryWeightInCarat += stone.getCarat() * stone.getAmount();
        }

        jewelryWeightInCarat += jewelry.getMetalWeight() * 5;
        jewelry.setJewelryTotalWeight(jewelryWeightInCarat);
    }

    public static void sortStonesByTotalCost(Jewelry jewelry) {
        System.out.println();
        System.out.println("Sort Stones by total cost: ");
        jewelry.getStoneList().sort(new Comparator<Stone>() {
            @Override
            public int compare(Stone o1, Stone o2) {
                return Double.compare(o1.getCost(), o2.getCost());
            }
        });
    }

    public static void sortStonesByCostPerCarat(Jewelry jewelry) {
        System.out.println();
        System.out.println("Sort Stones by cost per Carat:");
        jewelry.getStoneList().sort(new Comparator<Stone>() {
            @Override
            public int compare(Stone o1, Stone o2) {
                double costPerCaratStone1 = (o1.getCost() / o1.getAmount()) /  o1.getCarat();
                double costPerCaratStone2 = (o2.getCost() / o2.getAmount()) /  o2.getCarat();
                return Double.compare(costPerCaratStone1, costPerCaratStone2);
            }
        });
    }

    public static List<Stone> findStonesInGivenClarityRange(Jewelry jewelry, double lowerBound, double upperBound) {
        return jewelry.getStoneList().stream().filter(o ->
                o.getClarity() >= lowerBound && o.getClarity() <= upperBound).collect(Collectors.toList());
    }

    public static void printFoundStonesInClarityRange(List<Stone> stoneList) {
        stoneList.sort((o1, o2) -> Double.compare(o1.getClarity(), o2.getClarity()));
        if (stoneList.isEmpty()) {
            System.out.println("Stones not found in given clarity range!");
        } else {
            System.out.println();
            String format = "%-18s | %-15s | %-15s | %-8s | %-13s | %-6s | %-13s | %-13s \n";
            System.out.printf(format, "Name", "Color", "Country", "Carat", "Clarity(0-10)", "Amount", "Cost per carat", "Total Cost");
            System.out.print("------------------------------------------------------------------");
            System.out.print("------------------------------------------------------------------\n");

            for (Stone stone : stoneList) {
                System.out.println(stone);
            }
        }
    }

    public static void printJewelryStones(Jewelry jewelry) {
        System.out.println();
        System.out.printf("%-3s | %-18s | %-15s | %-15s | %-8s | %-13s | %-6s | %-13s | %-13s \n",
                "", "Name", "Color", "Country", "Carat", "Clarity(0-10)", "Amount", "Cost per carat", "Total Cost($)");
        System.out.print("------------------------------------------------------------------");
        System.out.print("------------------------------------------------------------------\n");
        for (int i = 0; i < jewelry.getStoneList().size(); i++) {
            System.out.println(String.format("%-3d | %s", (i + 1), jewelry.getStoneList().get(i)));
        }
    }

    public static void printJewelryTotalWeightAndCost(Jewelry jewelry) {
        System.out.println();
        System.out.printf("Total Jewelry cost = %.2f $, weight = %.2f (carat) \n", jewelry.getJewelryTotalCost(),
                jewelry.getJewelryTotalWeight());
    }

    public static void printJewelryMetalInformation(Jewelry jewelry) {
        System.out.println();
        System.out.printf("%-10s | %-13s | %10s \n", "Name", "Weight(gram)", "Cost($)");
        System.out.println("--------------------------------------------");
        System.out.printf("%-10s | %-13.2f | %10.2f $\n", jewelry.getMetal().name(), jewelry.getMetalWeight(),
                jewelry.getMetal().getCost());
    }

}
