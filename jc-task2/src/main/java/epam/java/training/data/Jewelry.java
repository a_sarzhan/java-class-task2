package epam.java.training.data;

import epam.java.training.jewelrystones.Stone;

import java.util.List;

public class Jewelry {
    private String name;
    private JewelryMetal metal;
    private double metalWeight;
    private double jewelryTotalCost;
    private double jewelryTotalWeight;
    private List<Stone> stoneList;

    public Jewelry(String name, JewelryMetal metal, double metalWeight, List<Stone> stones) {
        this.name = name;
        this.metal = metal;
        this.metalWeight = metalWeight;
        this.stoneList = stones;
    }

    public String getName() {
        return name;
    }

    public JewelryMetal getMetal() {
        return metal;
    }

    public double getMetalWeight() {
        return metalWeight;
    }

    public double getJewelryTotalCost() {
        return jewelryTotalCost;
    }

    public double getJewelryTotalWeight() {
        return jewelryTotalWeight;
    }

    public void setJewelryTotalCost(double jewelryTotalCost) {
        this.jewelryTotalCost = jewelryTotalCost;
    }

    public void setJewelryTotalWeight(double jewelryTotalWeight) {
        this.jewelryTotalWeight = jewelryTotalWeight;
    }

    public List<Stone> getStoneList() {
        return stoneList;
    }

}
