package epam.java.training.data;

public enum JewelryMetal {
    GOLD(53),
    SILVER(1),
    PLATINUM(30),
    PALLADIUM(90);

    private double cost;

    JewelryMetal(double cost) {
        this.cost = cost;;
    }

    public double getCost() {
        return cost;
    }

}
