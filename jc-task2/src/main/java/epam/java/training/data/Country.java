package epam.java.training.data;

public enum Country {
    RUSSIA(990, 30, 80),
    CANADA(780, 35, 120),
    ANGOLA(1100, 40, 90),
    USA(450, 40, 100),
    KONGO(630, 35, 150),
    CHINA(250, 30, 70),
    INDIA(350, 110, 90),
    AUSTRALIA(400, 120, 160),
    ITALY(660, 90, 200),
    RSA(1300, 80, 250),
    GERMANY(770, 50, 270),
    BRASILIA(230, 120, 170),
    NIGERIA(500, 70, 140);

    private int diamondMiningCost;
    private int pearlMiningCost;
    private int berylMiningCost;

    Country(int diamondMiningCost, int pearlMiningCost, int berylMiningCost) {
        this.diamondMiningCost = diamondMiningCost;
        this.pearlMiningCost = pearlMiningCost;
        this.berylMiningCost = berylMiningCost;
    }

    public int getDiamondMiningCost() {
        return diamondMiningCost;
    }

    public int getPearlMiningCost() {
        return pearlMiningCost;
    }

    public int getBerylMiningCost() {
        return berylMiningCost;
    }

}
