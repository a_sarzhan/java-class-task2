package epam.java.training.data;

public enum CuttingModel {
    POINT_CUT(130), TABLE_CUT(150), OLD_SINGLE_CUT(230), MAZARIN_CUT(275), PERUZZI_CUT(320), OLD_EUROPEAN_CUT(345);

    private double cost;

    CuttingModel(double cost) {
        this.cost = cost;
    }

    public double getCost() {
        return cost;
    }

}
