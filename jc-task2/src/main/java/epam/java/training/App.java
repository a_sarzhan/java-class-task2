package epam.java.training;

import epam.java.training.actions.JewelryAction;
import epam.java.training.actions.UserAction;
import epam.java.training.data.Jewelry;
import epam.java.training.data.JewelryMetal;
import epam.java.training.jewelrystones.Stone;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Epam - Java Web Development Training
 * Java Classes - Task2
 * Assel Sarzhanova
 */
public class App {

    public static void main( String[] args ) throws IOException {

        UserAction.printWelcomeScreen();
        List<Stone> stoneList = new ArrayList<>(UserAction.getUserChosenStones());

        String jewelryName = UserAction.getUserGivenJewelryName();
        JewelryMetal metal = UserAction.getUserChosenJewelryMetal();
        double metalWeight = UserAction.getUserDefinedMetalWeight();

        Jewelry jewelry = new Jewelry(jewelryName,metal, metalWeight, stoneList);
        JewelryAction.calculateAndSetJewelryTotalCost(jewelry);
        JewelryAction.calculateAndSetJewelryTotalWeight(jewelry);

        JewelryAction.printJewelryStones(jewelry);
        JewelryAction.printJewelryMetalInformation(jewelry);
        JewelryAction.printJewelryTotalWeightAndCost(jewelry);

        JewelryAction.sortStonesByTotalCost(jewelry);
        JewelryAction.printJewelryStones(jewelry);

        JewelryAction.sortStonesByCostPerCarat(jewelry);
        JewelryAction.printJewelryStones(jewelry);

        double clarityLowerBound = UserAction.getUserDefinedClarityLowerBound();
        double clarityUpperBound = UserAction.getUserDefinedClarityUpperBound();
        List<Stone> stoneInClarityRangeList = JewelryAction.findStonesInGivenClarityRange(jewelry, clarityLowerBound, clarityUpperBound);
        JewelryAction.printFoundStonesInClarityRange(stoneInClarityRangeList);

        UserAction.finishUserInteraction();

    }

}
