package epam.java.training.jewelrystones;

import java.util.Random;

public class Aquamarine extends Beryl {

    public Aquamarine(String name, double carat, String color, int amount) {
        super(name, carat, color, amount);
    }

    @Override
    public void generateHardness() {
        double randomHardness = new Random().nextDouble() * (10 - 7) + 7;
        setHardness(randomHardness);
    }

    @Override
    public void generateTenacity() {
        double randomTenacity = new Random().nextDouble() * 6;
        setTenacity(randomTenacity);
    }

    @Override
    public void defineStonesClarity() {
        double randomClarity = new Random().nextDouble() * (BEST_BERYL_CLARITY_LOWER_BOUND);
        setClarity(randomClarity);
    }

    @Override
    public double getCostDefinedByExpertsSurvey() {
        double aquamarinePreCost;
        double aquamarineLowestCost = 1000;
        double aquamarineHighestCost = 7000;

        if (getTenacity() < 3) {
            aquamarinePreCost = 0;
        } else {
            aquamarinePreCost = (getHardness() == 10) ? aquamarineHighestCost :
                    new Random().nextDouble() * (aquamarineHighestCost - aquamarineLowestCost) + aquamarineLowestCost;
        }
        return aquamarinePreCost * getCarat();
    }

}
