package epam.java.training.jewelrystones;

import java.util.Random;

public class Emerald extends Beryl {

    public Emerald(String name, double carat, String color, int amount) {
        super(name, carat, color, amount);
    }

    @Override
    public void generateHardness() {
        double randomHardness = new Random().nextDouble() * (10 - 7.5) + 7.5;
        setHardness(randomHardness);
    }

    @Override
    public void generateTenacity() {
        double randomTenacity = new Random().nextDouble() * (10 - 7.5) + 7.5;
        setTenacity(randomTenacity);
    }

    @Override
    public void defineStonesClarity() {
        double randomClarity = new Random().nextDouble() * (CLARITY_UPPER_BOUND - BEST_BERYL_CLARITY_LOWER_BOUND) + BEST_BERYL_CLARITY_LOWER_BOUND;
        setClarity(randomClarity);
    }

    @Override
    public double getCostDefinedByExpertsSurvey() {
        double emeraldLowestCost = 8000;
        double emeraldHighestCost = 16000;

        double emeraldPreCost = (getHardness() == 10 && getTenacity() == 10) ? emeraldHighestCost :
                new Random().nextDouble() * (emeraldHighestCost - emeraldLowestCost) +  emeraldLowestCost;
        return emeraldPreCost * getCarat();
    }

}
