package epam.java.training.jewelrystones;

import epam.java.training.data.CuttingModel;

import java.util.Random;

public class Diamond extends Stone {
    private CuttingModel cuttingModel;
    private static final int LOWEST_PRICE = 2000;
    private static final int MIDDLE_PRICE = 15000;
    private static final int HIGHEST_PRICE = 25000;

    private static final double DIAMOND_CLARITY_LOWER_BOUND = 8;

    public Diamond(String name, double carat, String color, int amount, CuttingModel cuttingModel){
        super(name, carat, color, amount);
        this.cuttingModel = cuttingModel;
    }

    @Override
    public void defineStonesClarity() {
        double randomClarity = new Random().nextDouble() * (CLARITY_UPPER_BOUND - DIAMOND_CLARITY_LOWER_BOUND)
                + DIAMOND_CLARITY_LOWER_BOUND;
        setClarity(randomClarity);
    }

    @Override
    public double getMiningExpense() {
        return getCountry().getDiamondMiningCost() * getCarat();
    }

    public double getMastersCuttingExpense(){
        return cuttingModel.getCost() * getCarat() * 100;
    }

    @Override
    public double getCostDefinedByExpertsSurvey() {
        double diamondPreCost;
        if (getClarity() == CLARITY_UPPER_BOUND) {
            diamondPreCost = new Random().nextDouble() * (HIGHEST_PRICE - MIDDLE_PRICE) + MIDDLE_PRICE;
        } else {
            diamondPreCost = new Random().nextDouble() * (MIDDLE_PRICE - LOWEST_PRICE) + LOWEST_PRICE;
        }
        return diamondPreCost * getCarat();
    }

}
