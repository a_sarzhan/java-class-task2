package epam.java.training.jewelrystones;

import epam.java.training.data.Country;

import java.util.Objects;

public abstract class Stone {
    static final double CLARITY_UPPER_BOUND = 10;
    private String name;
    private double carat;
    private String color;
    private double clarity;
    private double cost;
    private Country country;
    private int amount;

    Stone(String name, double carat, String color, int amount) {
        this.name = name;
        this.carat = carat;
        this.color = color;
        this.amount = amount;
    }

    public abstract double getMiningExpense();
    public abstract void defineStonesClarity();
    public abstract double getCostDefinedByExpertsSurvey();

    public void calculateNetCost(double ... allExpense) {
        double diamondCost = 0;
        for (double expense : allExpense) {
            diamondCost += expense;
        }
        diamondCost *= amount;
        setCost(diamondCost);
    }

    public int getAmount() {
        return amount;
    }

    void setClarity(double clarity) {
        this.clarity = clarity;
    }

    Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public double getCarat() {
        return carat;
    }

    String getColor() {
        return color;
    }

    public double getClarity() {
        return clarity;
    }

    public double getCost() {
        return cost;
    }

    private void setCost(double cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Stone)) return false;
        Stone stone = (Stone) o;
        return Double.compare(stone.getCarat(), getCarat()) == 0 &&
                Objects.equals(getName(), stone.getName()) &&
                Objects.equals(getColor(), stone.getColor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getCarat(), getColor());
    }

    @Override
    public String toString() {
        double costPerCarat = (cost / amount) / carat;
        return String.format("%-18s | %-15s | %-15s | %-8.2f | %-13.2f | %-6d | %13.2f $ | %13.2f $",
                                            name, color, country, carat, clarity, amount, costPerCarat, cost);
    }

}
