package epam.java.training.jewelrystones;

import java.util.Random;

public class Beryl extends Stone {
    private double tenacity;
    private double hardness;
    static final double BEST_BERYL_CLARITY_LOWER_BOUND = 6;

    public Beryl(String name, double carat, String color, int amount) {
        super(name, carat, color, amount);
    }

    public void generateHardness() {
        double randomHardness = new Random().nextDouble() * 10;
        setHardness(randomHardness);
    }

    public void generateTenacity() {
        double randomTenacity = new Random().nextDouble() * 10;
        setTenacity(randomTenacity);
    }

    void setTenacity(double tenacity) {
        this.tenacity = tenacity;
    }

    void setHardness(double hardness) {
        this.hardness = hardness;
    }

    double getTenacity() {
        return tenacity;
    }

    double getHardness() {
        return hardness;
    }

    @Override
    public double getMiningExpense() {
        return getCountry().getBerylMiningCost() * getCarat();
    }

    @Override
    public void defineStonesClarity() {
        double randomClarity;
        if (getColor().equalsIgnoreCase("green") || getColor().equalsIgnoreCase("blue")) {
            randomClarity = new Random().nextDouble() * (CLARITY_UPPER_BOUND - BEST_BERYL_CLARITY_LOWER_BOUND) + BEST_BERYL_CLARITY_LOWER_BOUND;
        } else {
            randomClarity = new Random().nextDouble() * (BEST_BERYL_CLARITY_LOWER_BOUND);
        }
        setClarity(randomClarity);
    }

    @Override
    public double getCostDefinedByExpertsSurvey() {
        double berylPreCost;
        double berylLowestCost = 50;
        double berylAverageCost = 5000;
        double berylHighestCost = 10000;

        if (tenacity > 4 && hardness > 6) {
            berylPreCost = new Random().nextDouble() * (berylHighestCost - berylAverageCost) + berylAverageCost;
        } else {
            berylPreCost = new Random().nextDouble() * (berylAverageCost - berylLowestCost) + berylLowestCost;
        }
        return berylPreCost * getCarat();
    }

}
