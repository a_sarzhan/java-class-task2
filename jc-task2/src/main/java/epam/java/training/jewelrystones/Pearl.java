package epam.java.training.jewelrystones;

import java.util.Random;

public class Pearl extends Stone {
    private int pearlAge;
    private double diameter;
    private static final int LOWEST_PRICE = 300;
    private static final int MIDDLE_PRICE = 900;
    private static final int HIGHEST_PRICE = 1500;

    public Pearl(String name, double carat, String color, int amount, double diameter, int pearlAge) {
        super(name, carat, color, amount);
        this.diameter = diameter;
        this.pearlAge = pearlAge;
    }

    @Override
    public double getMiningExpense() {
        return getCountry().getPearlMiningCost() * getCarat();
    }

    @Override
    public void defineStonesClarity() {
        setClarity(0);
    }

    public double getCostDefinedByExpertsSurvey() {
        double pearlPreCost;
        if (pearlAge > 6) {
            pearlPreCost = new Random().nextDouble() * (HIGHEST_PRICE - MIDDLE_PRICE) + MIDDLE_PRICE;
        } else {
            pearlPreCost = new Random().nextDouble() * (MIDDLE_PRICE - LOWEST_PRICE) + LOWEST_PRICE;
        }
        return pearlPreCost * getCarat() * (1 + diameter);
    }

}
