package epam.java.training.utility;

import epam.java.training.data.Country;
import epam.java.training.data.CuttingModel;
import epam.java.training.data.JewelryMetal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class InputValidator {
    private static final BufferedReader READER = new BufferedReader(new InputStreamReader(System.in));

    public static BufferedReader getREADER() {
        return READER;
    }

    public static String getValidString(String parameter) throws IOException {
        String userInputString;

        while (true) {
            System.out.println("Enter " + parameter + ": ");
            userInputString = READER.readLine();

            if (userInputString.isBlank()) {
                System.out.println("Blank user Input. Try again");
                continue;
            }
            break;
        }

        return userInputString;
    }

    public static int getValidInteger(String parameter) throws IOException {
        int userInputInteger;

        while (true) {
            try {
                System.out.println("Enter " + parameter + ": ");
                userInputInteger = Integer.parseInt(READER.readLine());

                if (userInputInteger < 0) {
                    System.out.println("Negative user Input. Try again");
                    continue;
                }

                break;

            } catch (NumberFormatException e) {
                System.out.println("Invalid user Input. Try again");
            }
        }

        return userInputInteger;
    }

    public static double getValidDouble(String parameter) throws IOException {
        double userInputDouble;

        while (true) {
            try {
                System.out.println("Enter " + parameter + ": ");
                userInputDouble = Double.parseDouble(READER.readLine());

                if (userInputDouble < 0) {
                    System.out.println("Negative user Input. Try again");
                    continue;
                }
                break;

            } catch (NumberFormatException e) {
                System.out.println("Invalid user Input. Try again");
            }
        }

        return userInputDouble;
    }

    public static CuttingModel getValidCuttingModel(String parameter) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        CuttingModel cuttingModel;
        String modelString;

        for (CuttingModel model : CuttingModel.values()) {
            stringBuilder.append(model.name().toLowerCase().replaceAll("_", " ") + ", ");
        }

        while (true) {
            try {
                System.out.println();
                System.out.println("Enter " + parameter + " cutting model from [" + stringBuilder.toString() + "]");

                modelString = READER.readLine().trim().toUpperCase().replaceAll("\\s+", "_");
                cuttingModel = CuttingModel.valueOf(modelString);
                break;

            } catch (IllegalArgumentException e) {
                System.out.println("Invalid Cutting Model. Try again");
            }
        }

        return cuttingModel;
    }

    public static Country getValidCountry(String parameter) throws IOException {
        Country stoneCountry;
        String countryString;
        StringBuilder stringBuilder = new StringBuilder();

        for (Country country : Country.values()) {
            stringBuilder.append(country.name().toLowerCase().replaceAll("_", " ") + ", ");
        }

        while (true) {
            try {
                System.out.println();
                System.out.println("Enter " + parameter + " producer country from [" + stringBuilder.toString() + "]");

                countryString = READER.readLine().trim().toUpperCase().replaceAll("\\s+", "_");
                stoneCountry = Country.valueOf(countryString);
                break;

            } catch (IllegalArgumentException e) {
                System.out.println("Invalid Country. Try again");
            }
        }

        return stoneCountry;
    }

    public static JewelryMetal getValidMetal() throws IOException {
        JewelryMetal metal;
        String metalString;
        StringBuilder stringBuilder = new StringBuilder();

        for (JewelryMetal jewelryMetal : JewelryMetal.values()) {
            stringBuilder.append(jewelryMetal.name().toLowerCase().replaceAll("_", " ") + ", ");
        }

        while (true) {
            try {
                System.out.println();
                System.out.println("Enter metal name from [" + stringBuilder.toString() + "]");

                metalString = READER.readLine().trim().toUpperCase().replaceAll("\\s+", "_");
                metal = JewelryMetal.valueOf(metalString);
                break;

            } catch (IllegalArgumentException e) {
                System.out.println("Invalid Metal. Try again");
            }
        }

        return metal;
    }

}
